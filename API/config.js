module.exports = {
    dbOptions: {
        host: "", // your host name
        user: "", // your Databasse User
        password: "", // your Databasse Password
        port: 3306,
        database: "", // your Databasse database
        dateStrings: true
    }
};