module.exports = (success, message, message_th) => {
    return ({
        success: success ? "success" : "error",
        message: message,
        message_th: message_th
    });
}