const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const myConnection = require("express-myconnection");

const config = require("./config");
const PORT = 3026;

app.use(cors());
app.use(
	bodyParser.json({
		limit: "50mb"
	})
);

app.use(myConnection(mysql, config.dbOptions, "pool"));

app.listen(PORT, () => {
	console.log("ready server on Port:" + PORT);
});

app.post("/API/select", (req, res) => {
	var {
		body
	} = req;

	req.getConnection((err, connection) => {
		var sql = "SELECT * FROM talk.data ORDER BY id DESC";

		connection.query(sql, (err, results) => {
			if (err) return next(err);
			res.json({
				success: "success",
				data: results
			});
		});
	});
});

app.post("/API/insert", (req, res) => {
	var {
		body
	} = req;

	req.getConnection((err, connection) => {
		var sql = "INSERT INTO data (date, temp, humid) VALUES (?, ?, ?);";

		connection.query(sql, [new Date(), body.temp, body.humid], (err, _) => {
			if (err) return next(err);
			res.json({
				success: "success"
			});
		});
	});
});
