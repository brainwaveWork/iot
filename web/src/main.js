import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

// bootstrap
import { BootstrapVue } from 'bootstrap-vue'
Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Mqtt
import VueMqtt from 'vue-mqtt';
Vue.use(VueMqtt, 'ws://localhost:11883', { clientId: 'WebClient-' + parseInt(Math.random() * 100000) });

new Vue({
  render: h => h(App),
}).$mount('#app')
